from django.shortcuts import render, redirect
from accounts.form import UserRegistrationForm
from django.contrib.auth import authenticate, login


def register(request):
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")
            form.save()
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    form = UserRegistrationForm()
    context = {"form": form}
    return render(request, "registration/signup.html", context)
