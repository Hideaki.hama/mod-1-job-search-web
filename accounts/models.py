from django.db import models
from django.contrib.auth.models import AbstractUser



class User(AbstractUser):
  is_employer = models.BooleanField(default=False)

class Employer(models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)