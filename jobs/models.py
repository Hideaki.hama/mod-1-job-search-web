from django.db import models
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL

class Job(models.Model):
    company = models.CharField(max_length=125)
    author = models.ForeignKey(
        USER_MODEL,
        related_name = "job",
        on_delete = models.CASCADE,
        null = True,
    )
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    hiring = models.FloatField(null=True)


    def __str__(self):
        return self.name + " by " + str(self.author)




class MyJobList(models.Model):
    user = models.ForeignKey(
        USER_MODEL,
        related_name = "job_lists",
        on_delete= models.CASCADE,
        null = True
        )
