from django.urls import path
from jobs.views import (
    JobCreateView,
    JobDeleteView,
    JobUpdateView,
    JobDetailView,
    JobListView,
)

urlpatterns = [
    path("", JobListView.as_view(), name="job_list"),
    path("<int:pk>/", JobDetailView.as_view(), name="job_detail"),
    path("<int:pk>/delete/", JobDeleteView.as_view(), name="job_delete"),
    path("new/", JobCreateView.as_view(), name="job_new"),
    path("<int:pk>/edit/", JobUpdateView.as_view(), name="job_edit"),
]