from django.shortcuts import render
from django.http.response import Http404, HttpResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods


from jobs.models import Job

def home_view(request):
  return render(request, 'jobs\homepage.html')



class JobListView(ListView):
    model = Job
    template_name = "jobs/list.html"
    paginate_by = 6


class JobDetailView(LoginRequiredMixin, DetailView):
    model = Job
    template_name = "jobs/detail.html"



class JobCreateView(LoginRequiredMixin, CreateView):
    model = Job
    template_name = "jobs/new.html"
    fields = ["company", "description", "image", "hiring"]
    success_url = reverse_lazy("job_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class JobUpdateView(LoginRequiredMixin, UpdateView):
    model = Job
    template_name = "jobs/edit.html"
    fields = ["company", "description", "image", "hiring"]


    def get_success_url(self):
        return reverse("job_detail", kwargs={"pk": self.object.pk})

class JobDeleteView(LoginRequiredMixin, DeleteView):
    model = Job
    template_name = "jobs/delete.html"
    success_url = reverse_lazy("job_list")
