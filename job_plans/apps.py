from django.apps import AppConfig


class JobPlansConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'job_plans'
